import { HAND_NORMAL } from '~/lib/constants';

export default () => ({
  useGrayscaleCSS: false,
  activeMap: 'firstScreen',
  cursor: HAND_NORMAL,
  lightsOnCount: 0,
  lightsOn: false,
  inventory: [],
  inventoryById: {},
  enabledInstruments: [],
  selection: [],
  message: '',
  visible: [],
  visited: ['firstScreen']
});
