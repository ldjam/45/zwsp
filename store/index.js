import state from './state';
import * as actions from './actions';
import * as getters from './getters';
import mutations from './mutations';

export const createStoreOptions = () => ({
  state: state(),
  actions,
  mutations,
  getters
});

export default createStoreOptions();
