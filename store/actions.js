import { flatten } from 'lodash';
import * as types from './mutation_types';
import { HAND_NORMAL, HAND_POINTER } from '~/lib/constants';
import { getToggleLightsMessage } from '~/lib/messages';
import {
  getItemMessage,
  getItemPickUpMessage,
  getItemSocketFailMessage,
  items,
  getItemSocketSuccessMessage
} from '~/lib/items';
import { getMapMessage } from '~/lib/maps';
import { findRecipe, matchesRecipe } from '~/lib/alchemy';

export const useGrayscaleCSS = ({ commit }) => {
  commit(types.USE_GRAYSCALE_CSS);
};

export const navigateTo = ({ commit, dispatch, state }, mapName) => {
  dispatch('setMessage', getMapMessage(mapName, state.visited));
  commit(types.SET_ACTIVE_MAP, mapName);
  dispatch('playSound', 'navigation');
};

export const setCursorPointer = ({ commit }) => {
  commit(types.SET_CURSOR, HAND_POINTER);
};

export const setCursorNormal = ({ commit }) => {
  commit(types.SET_CURSOR, HAND_NORMAL);
};

export const toggleLights = ({ state, commit, dispatch }) => {
  commit(types.TOGGLE_LIGHTS);

  dispatch('setMessage', getToggleLightsMessage(state));
  if (state.lightsOn) {
    dispatch('playSound', 'switchOn');
  }
};

export const pickUp = ({ commit, dispatch, state }, name) => {
  commit(types.ADD_TO_INVENTORY, name);
  dispatch('setMessage', getItemPickUpMessage(name, state));
  dispatch('playSound', 'pickUpItem');
};

export const destroyItem = ({ commit }, id) => {
  commit(types.CLEAR_SELECTION);
  commit(types.REMOVE_FROM_INVENTORY, id);
};

export const consumeFromSelection = ({ dispatch, getters }, names) => {
  const consumptions = flatten(
    names.map((name) => getters.selection.filter((x) => x.name === name))
  );

  consumptions.forEach(({ id }) => dispatch('destroyItem', id));
};

export const unselect = ({ dispatch, commit }) => {
  dispatch('setMessage', '');
  commit(types.CLEAR_SELECTION);
};

export const selectItem = ({ state, dispatch, commit, getters }, id) => {
  if (state.selection.includes(id)) {
    dispatch('unselect');
    return;
  }

  commit(types.ADD_TO_SELECTION, id);

  if (state.selection.length === 1) {
    dispatch('playItemMessage', { type: 'onSelect', id });
    return;
  }

  const recipe = findRecipe(getters.selectionNames);

  if (!recipe) {
    dispatch('playItemMessage', {
      type: 'onRecipeFail',
      id: state.selection[0]
    });
    commit(types.CLEAR_SELECTION);
    dispatch('playSound', 'alchemyFailed');
  } else if (matchesRecipe(recipe, getters.selectionNames)) {
    dispatch('consumeFromSelection', recipe.consumes || []);
    commit(types.CLEAR_SELECTION);
    dispatch('pickUp', recipe.result);
    dispatch('playSound', 'alchemySuccess');
  } else {
    dispatch('setMessage', 'Hmmm... What else can I add to this?');
  }
};

export const setMessage = ({ commit }, message) => {
  commit(types.SET_MESSAGE, message);
};

export const playItemMessage = ({ state, dispatch }, { id, type }) => {
  const item = state.inventoryById[id];

  const message = getItemMessage(item.name, type);

  dispatch('setMessage', message);
};

export const useSocket = (
  { getters, commit, dispatch },
  { socketSwitch, requires = [] }
) => {
  if (!requires.every((item) => getters.selectionNames.includes(item))) {
    dispatch('setMessage', getItemSocketFailMessage(socketSwitch, requires));
    commit(types.CLEAR_SELECTION);
    return;
  }

  dispatch('consumeFromSelection', requires);
  commit(types.USE_SOCKET, socketSwitch);

  dispatch('setMessage', getItemSocketSuccessMessage(socketSwitch, requires));
  dispatch('playSound', 'socket');
};

export const playSound = (_, soundName) => {
  // just a placeholder, see SoundScene
};

export const toggleMusic = ({ commit }, instrument) => {
  commit(types.TOGGLE_MUSIC, instrument);
};

export const restart = () => {
  location.reload();
};

export const cheat = ({ dispatch }) => {
  if (window.DO_YOU_LIKE_JAMS === 'yes') {
    Object.keys(items).forEach((item) => dispatch('pickUp', item));
  }
};
