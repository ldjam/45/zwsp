import { groupBy } from 'lodash';

export const selectionById = (state) =>
  state.selection.reduce((acc, key) => ({ ...acc, [key]: true }), {});

export const selectionNames = (state, getters) =>
  getters.selection.map((x) => x.name);

export const hasNoColors = (state, getters) =>
  Object.keys(getters.enabledColors).every(
    (key) => !getters.enabledColors[key]
  );

export const enabledColors = (state) => ({
  red: state.visible.includes('red'),
  green: state.visible.includes('green'),
  blue: state.visible.includes('blue')
});

export const selection = (state) =>
  state.selection.map((id) => state.inventoryById[id]);

export const inventory = (state) =>
  state.inventory.map((id) => state.inventoryById[id]);

export const inventoryByName = (state, getters) =>
  groupBy(getters.inventory, (x) => x.name);
