import Vue from 'vue';
import { uniqueId } from 'lodash';
import * as types from './mutation_types';

export default {
  [types.USE_GRAYSCALE_CSS](state) {
    state.useGrayscaleCSS = true;
  },
  [types.TOGGLE_LIGHTS](state) {
    state.lightsOn = !state.lightsOn;

    if (state.lightsOn) {
      state.lightsOnCount += 1;
    }
  },
  [types.ADD_TO_INVENTORY](state, name) {
    const id = uniqueId('item');
    Vue.set(state.inventoryById, id, {
      id,
      name
    });
    state.inventory.push(id);
  },
  [types.REMOVE_FROM_INVENTORY](state, id) {
    const index = state.inventory.indexOf(id);
    state.inventory.splice(index, 1);

    if (state.inventoryById[id]) {
      Vue.delete(state.inventoryById, id);
    }
  },
  [types.ADD_TO_SELECTION](state, id) {
    const existing = state.selection.find((x) => x === id);

    if (!existing) {
      state.selection.push(id);
    }
  },
  [types.CLEAR_SELECTION](state) {
    state.selection = [];
  },
  [types.SET_MESSAGE](state, message) {
    state.message = message;
  },
  [types.SET_ACTIVE_MAP](state, mapName) {
    state.visited.push(mapName);
    state.activeMap = mapName;
  },
  [types.SET_CURSOR](state, value) {
    state.cursor = value;
  },
  [types.TOGGLE_MUSIC](state, instrument) {
    const { enabledInstruments } = state;
    if (enabledInstruments.includes(instrument)) {
      state.enabledInstruments = enabledInstruments.filter(
        (i) => i !== instrument
      );
    } else {
      state.enabledInstruments = [...enabledInstruments, instrument];
    }
  },
  [types.USE_SOCKET](state, key) {
    state.visible.push(key);
  }
};
