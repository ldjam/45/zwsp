import Phaser from 'phaser';
import WallScene from './scenes/wall-scene';

import { WALL_WIDTH } from './constants';
import { MAPS } from './maps';
import GrayscalePipeline from './grayscale-pipeline';
import SoundScene from '~/lib/scenes/sound-scene';

const ZOOM = 0.25;

const config = {
  type: Phaser.AUTO,
  width: WALL_WIDTH,
  height: 2480,
  pixelArt: true,
  physics: {
    default: 'arcade',
    arcade: {
      gravity: { y: 0 }
    }
  },
  input: {
    mouse: true
  },
  scale: {
    zoom: ZOOM
  }
};

function setupResize(game) {
  // keep game the same size as the canvas element
  const resize = () => {
    game.scale.resize(
      parseInt(game.canvas.style.width, 10) / ZOOM,
      parseInt(game.canvas.style.height, 10) / ZOOM
    );
  };
  window.addEventListener('resize', resize);
  resize();
}

function setupScenes(game, vm) {
  const baseUrl = vm.$router.options.base;
  const store = vm.$store;

  const soundScene = new SoundScene(store);
  game.scene.add(soundScene.key, soundScene, true);

  Object.keys(MAPS).forEach((mapName) => {
    const scene = new WallScene(baseUrl, store, mapName, MAPS[mapName]);
    game.scene.add(mapName, scene);
  });

  store.watch(
    () => store.state.activeMap,
    (activeKey, oldKey) => {
      // unload previous one
      if (oldKey) {
        game.scene.switch(oldKey, activeKey);
      } else if (!game.scene.isActive(activeKey)) {
        game.scene.start(activeKey);
      }
    },
    { immediate: true }
  );
}

function setupGrayscaleWebGL(game) {
  game.renderer.addPipeline('Grayscale', new GrayscalePipeline(game));
}

function setupGrayscaleCanvas(game, store) {
  store.dispatch('useGrayscaleCSS');
}

function setupGrayscale(game, store) {
  if (game.config.renderType === Phaser.CANVAS) {
    setupGrayscaleCanvas(game, store);
  } else {
    setupGrayscaleWebGL(game);
  }
}

export default function main(vm) {
  const game = new Phaser.Game({
    ...config,
    scene: {},
    parent: vm.$refs.main
  });

  setupScenes(game, vm);
  setupResize(game);
  setupGrayscale(game, vm.$store);

  return game;
}
