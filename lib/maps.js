import desk from '~/static/assets/desk.json';
import exit from '~/static/assets/exit.json';
import firstScreen from '~/static/assets/first_sceen.json';
import kitchen from '~/static/assets/kitchen.json';
import endScreen from '~/static/assets/end_screen.json';

export const MAPS = {
  desk,
  exit,
  firstScreen,
  kitchen,
  endScreen
};

const mapMessages = {
  desk: {},
  exit: {},
  firstScreen: {},
  kitchen: {
    entry: 'What is in the kitchen?'
  },
  endScreen: {}
};

export const getMapMessage = (mapName, visited) => {
  const messages = mapMessages[mapName];

  if (visited.includes(mapName)) {
    return messages.random;
  } else {
    return messages.entry;
  }
};
