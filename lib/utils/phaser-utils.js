export const getObjectDataValue = (obj, name) => {
  const dataValues = obj && obj.data && obj.data.values;
  const values = Object.keys(dataValues || {}).map((key) => dataValues[key]);

  const prop = values.find((x) => x.name === name);

  return prop && prop.value;
};

export const getObjectData = (obj, keyObj = {}) => {
  if (Array.isArray(keyObj)) {
    const newKeyObj = keyObj.reduce((acc, key) => ({ ...acc, [key]: key }), {});
    return getObjectData(obj, newKeyObj);
  }

  return Object.keys(keyObj)
    .map((key) => ({ key, value: getObjectDataValue(obj, keyObj[key]) }))
    .reduce(
      (acc, { key, value }) => ({
        ...acc,
        [key]: value
      }),
      {}
    );
};
