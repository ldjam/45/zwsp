export const randomPick = (arr) => {
  const idx = Math.floor(Math.random() * arr.length);

  return arr[idx];
};

export const randomPicker = (arr) => {
  let lastPick;

  return () => {
    const available = arr.filter((x) => x !== lastPick);

    const pick = randomPick(available.length ? available : arr);

    lastPick = pick;

    return pick;
  };
};

export const toSet = (items) => (items instanceof Set ? items : new Set(items));

export const isSubset = (items1, items2) => {
  const set1 = toSet(items1);
  const set2 = toSet(items2);

  return Array.from(set1).every((item) => set2.has(item));
};

export const isEqualSets = (items1, items2) => {
  const set1 = toSet(items1);
  const set2 = toSet(items2);

  if (set1.size !== set2.size) {
    return false;
  }

  return Array.from(set1).every((item) => set2.has(item));
};

export const isEqualArrays = (items1, items2) => {
  if (items1.length !== items2.length) {
    return false;
  }

  return items1.every((val, idx) => val === items2[idx]);
};
