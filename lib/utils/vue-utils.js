import Vue from 'vue';

export const createReactiveObject = (data = {}) => {
  const tempVm = new Vue({
    data() {
      return { data };
    }
  });

  return tempVm.data;
};
