export const TYPE_INVENTORY = 'inventory';
export const TYPE_LIGHT_SWITCH = 'light_switch';
export const TYPE_CLICKABLE = 'clickable';
export const TYPE_SOCKET = 'socket';
export const WALL_WIDTH = 3508;

export const HAND_NORMAL = 'hand_normal';
export const HAND_POINTER = 'hand_pointer';
export const HAND_URLS = {
  [HAND_NORMAL]: 'assets/hand_1.png',
  [HAND_POINTER]: 'assets/hand_2.png'
};
