import { randomPicker } from './utils/array-utils';

const defaultMessages = {
  onPickUp: () => 'Oh cool! A thing!',
  onSelect: () => '',
  onRecipeFail: () => "Well that didn't work...",
  onSocketFail: () => 'This looks like a good place to put something.',
  onSocketSuccess: () => "Alright! Now we're getting somewhere."
};

const defaultItem = {
  image: '',
  message: {
    ...defaultMessages.onPickUp
  }
};

export const items = {
  square_pink: {
    image: 'assets/square_pink.png',
    messages: {
      onPickUp: () => 'A pink square! Now I can practice my origami.',
      onSelect: () => 'Hey there pink square!',
      onRecipeFail: () =>
        "I don't think I can use this pink square like that..."
    }
  },
  scissors: {
    image: 'assets/scissors.png',
    messages: {
      onPickUp: () =>
        "Safety first! I can't just leave scissors hanging around. I better put them in my pocket.",
      onSelect: () => 'Now what should I cut... What should I cut...',
      onRecipeFail: () =>
        "I don't want to cut this, I might need it for something else..."
    }
  },
  triangles_pink: {
    image: 'assets/items/item_triangles.png',
    messages: {
      onPickUp: () => 'Take that pink square!',
      onSelect: () =>
        "Don't worry little guys. I'll find you a nice pointy three sided home.",
      onSocketFail: () => 'So sad. Three sides, but nothing inside...',
      onSocketSuccess: () =>
        'Alright! Time to start looking around for another wall.'
    }
  },
  ruler: {
    image: 'assets/ruler.png',
    messages: {
      onPickUp: () => "Okay. Let's see how this game measures up.",
      onSelect: randomPicker([
        'As ruler of this land, I...',
        'This rules!',
        'So let me get this straight...',
        'No ruler puns please! That is where I draw the line!'
      ]),
      onRecipeFail: () =>
        "I'd say this is about the size one would expect this thing to be."
    }
  },
  tomatoSeed: {
    image: 'assets/tomato_seed.png',
    messages: {}
  },
  chemicalBlue: {
    image: 'assets/items/item_chemical_blue.png',
    messages: {
      onPickUp: () =>
        'Oh, an unidentified blue chemical... I better take this.',
      onSelect: () =>
        'Randomly mixing chemicals together sounds like a good idea...'
    }
  },

  chemicalGreen: {
    image: 'assets/items/item_chemical_green.png',
    messages: {
      onPickUp: () =>
        "It's an unidentified green chemical... It smells bad, but I'm going to take it.",
      onSelect: () =>
        'Randomly mixing chemicals together sounds like a good idea...'
    }
  },

  chemicalCyan: {
    image: 'assets/items/item_chemical_cyan.png',
    messages: {
      onPickUp: () =>
        "Oh, it's now an undefinied cyan chemical. I think, that's better.",
      onSelect: () =>
        'Randomly mixing chemicals together sounds like a good idea...'
    }
  },

  chemicalRed: {
    image: 'assets/items/item_chemical_red.png',
    messages: {
      onPickUp: () =>
        'Mmm! Smells like strawberry juice. But, it is an unidentified red chemical. I better not drink too much.',
      onSelect: () =>
        'Randomly mixing chemicals together sounds like a good idea...'
    }
  },

  chemicalPurple: {
    image: 'assets/items/item_chemical_purple.png',
    messages: {
      onPickUp: () => 'How nice! What a beautiful color!',
      onSelect: () =>
        'Randomly mixing chemicals together sounds like a good idea...'
    }
  },

  chemicalBrown: {
    image: 'assets/items/item_chemical_brown.png',
    messages: {
      onPickUp: () => 'Urgh... it smells not better and now it looks bad...',
      onSelect: () =>
        'Perhaps I can mix this with another chemical to make it smell better?'
    }
  },

  chemicalBlack: {
    image: 'assets/items/item_chemical_black.png',
    messages: {
      onPickUp: () => "Oh... it's black! Hm... it seems to be acid.",
      onSelect: () =>
        'I can feel this burning through my pocket! I better find a place to put it - and fast!'
    }
  },

  frog: {
    image: 'assets/items/item_frog.png',
    messages: {
      onPickUp: () => 'A slimy frog prince.',
      onSelect: () =>
        "I wonder if this frog prince will turn into a wonderful Prince if I kiss him... I guess we'll never know (too slimy)! >_<"
    }
  },

  squashedTomato: {
    image: 'assets/items/item_squashed_tomato.png',
    messages: {
      onPickUp: () => 'Oh no this tomato is squashed...',
      onSelect: () => 'Hm… I can only make soup or jam with this…',
      onSocketSuccess: () => 'Ah! Just like Mom used to make...',
      onRecipeFail: () =>
        "I'm not sure I want to do this - especially in my pocket!"
    }
  },

  tomato: {
    image: 'assets/items/item_tomato.png',
    messages: {
      onPickUp: () => 'A wonderful red tomato!',
      onSelect: () => 'A wonderful red tomato!'
    }
  },

  broom: {
    image: 'assets/items/item_broom.png',
    messages: {
      onPickUp: () => 'Get a broom. Now you can clean up this room.',
      onSelect: () => 'A broom.'
    }
  },

  sugar: {
    image: 'assets/items/item_sugar.png',
    messages: {
      onPickUp: () => 'This is sugar (not to be confused with salt).',
      onSelect: () => 'So sweet!',
      onSocketSuccess: () =>
        'Beautiful! Next the recipe says "Let it sit for a few minutes"...'
    }
  },

  bug: {
    image: 'assets/items/item_bug.png',
    messages: {
      onPickUp: () =>
        'Oh a green bug! The encyclopedia was right. Green bugs (and I) love jam.',
      onSelect: () => 'A green bug.',
      onRecipeFail: () =>
        'The bug flails wildly, thankfully I caught her before she escaped my pocket.'
    }
  },

  blueprint: {
    image: 'assets/items/item_blueprint_insertable.png',
    messages: {
      onPickUp: () => 'Who inserted a blueprint in a typewriter?',
      onSelect: () =>
        "Now according to this blueprint... It looks like I can build a rocket... theoretically... ^-^' "
    }
  },

  emptyBottle: {
    image: 'assets/items/item_empty_bottle.png',
    messages: {
      onPickUp: () => 'A bottle with nothing in it.',
      onSelect: () => 'Perhaps I can store something in this bottle?',
      onSocketFail: () =>
        "I can't just put this in my pocket. It will make my pants all wet."
    }
  },

  bottleWithWater: {
    image: 'assets/items/item_bottle_with_water.png',
    messages: {
      onPickUp: () => 'Now it fits better into my pocket.',
      onSelect: () => 'A bottle with clear water.'
    }
  },

  milk: {
    image: 'assets/items/item_milk.png',
    messages: {
      onPickUp: () => 'Milk contains a lot of calcium.',
      onSelect: randomPicker([
        "I don't want to drink milk right now.",
        'Why do cows have strong bones? Because they have a lot of COWcium.'
      ])
    }
  },

  flour: {
    image: 'assets/items/item_flour.png',
    messages: {
      onPickUp: () => "It's a wheat flour.",
      onSelect: () => 'I can make bread or cake with it.'
    }
  },

  egg: {
    image: 'assets/items/item_egg.png',
    messages: {
      onPickUp: () => "It's a egg.",
      onSelect: randomPicker([
        'What can I do with this egg?',
        'Ah! Eggscelent!'
      ])
    }
  },

  piePlate: {
    image: 'assets/items/item_pie_plate.png',
    messages: {
      onPickUp: () => "It's a pie plate.",
      onSelect: () => 'Perhaps I can make pie.'
    }
  },

  frogPie: {
    image: 'assets/items/item_frog_pie.png',
    messages: {
      onPickUp: () => 'Mhhh... a frog pie!',
      onSelect: () => "Hm... I'm not sure if it taste good..."
    }
  },

  collectibleSoil: {
    image: 'assets/items/item_soil_to_collect.png',
    messages: {
      onPickUp: () => "Oh good, it's a nutritious soil.",
      onSelect: () => "Hm... I'm not sure if it taste good...",
      onRecipeFail: () =>
        "I don't want to mix this in my pocket. Maybe I can put this somewhere first?"
    }
  },

  woolRed: {
    image: 'assets/items/item_wool_red.png',
    messages: {
      onPickUp: () => "It's red wool.",
      onSelect: () => "It's red wool."
    }
  },

  woolGreen: {
    image: 'assets/items/item_wool_green.png',
    messages: {
      onPickUp: () => "It's green wool.",
      onSelect: () => "It's green wool."
    }
  },

  woolBlue: {
    image: 'assets/items/item_wool_blue.png',
    messages: {
      onPickUp: () => "It's blue wool.",
      onSelect: () => "It's blue wool."
    }
  },

  straw: {
    image: 'assets/items/item_straw.png',
    messages: {
      onPickUp: () => 'Who put here straw?',
      onSelect: () => 'Hm... what can I make with straw?'
    }
  },

  voodooDoll: {
    image: 'assets/items/item_voodoo_doll.png',
    messages: {
      onPickUp: () => 'Harharhar, I make a Voodoo Doll!',
      onSelect: () => 'Harharhar, I make a Voodoo Doll!'
    }
  },

  sweater: {
    image: 'assets/items/item_sweater.png',
    messages: {
      onPickUp: () => 'I make a sweater.',
      onSelect: () => "It's an ugly sweater. Who will wear such sweater?"
    }
  },

  buttons: {
    image: 'assets/items/item_buttons.png',
    messages: {
      onPickUp: () => 'Oh buttons!',
      onSelect: () => 'Hm... nothig happen when I click them...'
    }
  }
};

export const getItem = (key) => items[key] || defaultItem;

export const getItemMessage = (key, type) => {
  const item = getItem(key);

  const messageFn =
    (item.messages && item.messages[type]) || defaultMessages[type];

  return (messageFn && messageFn()) || '';
};

export const getItemPickUpMessage = (key) => {
  return getItemMessage(key, 'onPickUp');
};

export const getItemSelectMessage = (key) => {
  return getItemMessage(key, 'onSelect');
};

export const getItemRecipeFailMessage = (key) => {
  return getItemMessage(key, 'onRecipeFail');
};

export const getItemSocketFailMessage = (socketSwitch, requires) => {
  return getItemMessage(requires[0] || '', 'onSocketFail');
};

export const getItemSocketSuccessMessage = (socketSwitch, requires) => {
  return getItemMessage(requires[0] || '', 'onSocketSuccess');
};
