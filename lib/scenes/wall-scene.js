import Phaser from 'phaser';
import { setup } from './setup';

export default class WallScene extends Phaser.Scene {
  constructor(baseUrl, store, mapName, mapData) {
    super({ key: mapName });
    this.store = store;
    this.baseUrl = baseUrl;
    this.mapData = mapData;
  }

  /**
   * @param layerName {String}
   * @returns {String}
   */
  imageKey(layerName) {
    return `${this.mapName}-image-${layerName}`;
  }

  /**
   * @param gid {Number} tile ID (unique per map)
   * @returns {String}
   */
  tilesetKey(gid) {
    // assumption: our tilesets contain only one tile
    return `${this.mapName}-tileset-${gid}`;
  }

  /**
   * @returns {String}
   */
  get mapName() {
    return this.sys.config.key;
  }

  preload() {
    this.load.setBaseURL(this.baseUrl);

    const { layers, tilesets } = this.mapData;

    layers
      .filter(({ type }) => type === 'imagelayer')
      .map(({ image, name }) => ({ image, key: this.imageKey(name) }))
      .concat(
        tilesets.map(({ image, firstgid }) => ({
          image,
          key: this.tilesetKey(firstgid)
        }))
      )
      .forEach(({ image, key }) => {
        this.load.image(key, `assets/${image}`);
      });

    this.load.tilemapTiledJSON(this.mapName, this.mapData);
  }

  create() {
    const setupObject = setup(this);
    const map = this.make.tilemap({ key: this.mapName });

    map.tilesets.forEach(({ name, firstgid }) =>
      map.addTilesetImage(name, this.tilesetKey(firstgid))
    );

    map.images.forEach((layer) => {
      const { x, y, name } = layer;
      const image = this.add.image(x, y, this.imageKey(name));
      image.setOrigin(0, 0);
    });

    map.objects.forEach(({ name: objectLayerName, objects }) =>
      objects.forEach(({ gid, name }) => {
        if (!gid) {
          return;
        }

        const [obj] = map.createFromObjects(objectLayerName, name, {
          key: this.tilesetKey(gid)
        });

        setupObject(obj);
      })
    );
  }

  update() {}
}
