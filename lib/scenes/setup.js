import Phaser from 'phaser';
import { getObjectData } from '../utils/phaser-utils';
import { useLights } from '../behaviors/lights';
import { useFromType } from '../behaviors/from-type';
import { toggleVisible } from '../behaviors/visibility-toggle-target';
import { toggleVisibleFromColor } from '../behaviors/colorable';

const createExtendedGameObject = (obj) => {
  const disposables = [];

  obj.on('destroy', () => {
    disposables.forEach((dispose) => dispose());
  });

  obj.disposable = (fn) => {
    disposables.push(fn);
  };

  return obj;
};

export const setup = (scene) => {
  if (scene.game.config.renderType === Phaser.WEBGL) {
    scene.cameras.main.setRenderToTexture('Grayscale');

    scene.store.watch(
      () => scene.store.getters.hasNoColors,
      (hasNoColors) => {
        // TODO: Woah! Double negative!
        if (!hasNoColors) {
          scene.cameras.main.renderToTexture = false;
        }
      },
      { immediate: true }
    );
  }

  scene.events.on('create', () => {
    useLights(scene);
  });

  scene.input
    .on('pointerover', () => {
      scene.store.dispatch('setCursorPointer');
    })
    .on('pointerout', () => {
      scene.store.dispatch('setCursorNormal');
    });

  return (origObj) => {
    const obj = createExtendedGameObject(origObj);

    const {
      type = '',
      hidden = false,
      visibilityToggleTarget = '',
      color = null
    } = getObjectData(obj, [
      'type',
      'hidden',
      'visibilityToggleTarget',
      'color'
    ]);

    obj.visible = !hidden && !color;

    if (visibilityToggleTarget) {
      toggleVisible(scene, obj);
    }
    if (color) {
      toggleVisibleFromColor(scene, obj, color);
    }
    useFromType(type)(scene, obj);
  };
};
