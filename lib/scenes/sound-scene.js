import Phaser from 'phaser';

const musicFiles = {
  congas: { fileName: 'congas', volume: 0.1 },
  marimba: { fileName: 'marimba', volume: 0.1 },
  piano: { fileName: 'piano', volume: 0.1 },
  trombones: { fileName: 'trombones', volume: 0.1 }
};

const soundFiles = {
  alchemyFailed: { fileName: 'alchemy-failed', volume: 0.6 },
  alchemySuccess: { fileName: 'alchemy-success' },
  navigation: { fileName: 'navigation' },
  pickUpItem: { fileName: 'pick-up-item' },
  socket: { fileName: 'socket' },
  switchOn: { fileName: 'switch' }
};

const musicKey = (instrument) => `music-${instrument}`;
const soundKey = (soundName) => `sound-${soundName}`;

export default class SoundScene extends Phaser.Scene {
  constructor(store) {
    super({ key: 'music' });
    this.store = store;
  }

  get key() {
    return this.sys.config.key;
  }

  preload() {
    Object.entries(musicFiles).forEach(([instrument, { fileName }]) => {
      this.load.audio(musicKey(instrument), [
        `assets/music/${fileName}.ogg`,
        `assets/music/${fileName}.mp3`
      ]);
    });

    Object.entries(soundFiles).forEach(([soundName, { fileName }]) => {
      this.load.audio(soundKey(soundName), [
        `assets/sounds/${fileName}.ogg`,
        `assets/sounds/${fileName}.mp3`
      ]);
    });
  }

  create() {
    const music = {};
    Object.keys(musicFiles).forEach((instrument) => {
      music[instrument] = this.sound.add(musicKey(instrument));
    });

    const sounds = {};
    Object.keys(soundFiles).forEach((soundName) => {
      sounds[soundName] = this.sound.add(soundKey(soundName));
    });

    this.store.watch(
      (state) => state.enabledInstruments,
      (enabledInstruments) => {
        // make all instruments play synchronously
        Object.values(music).forEach((m) => m.stop());

        enabledInstruments.forEach((instrument) => {
          const { volume = 1.0 } = musicFiles[instrument];
          music[instrument].play({
            loop: true,
            volume
          });
        });
      }
    );

    // we cannot use a watcher here because playing the same sound twice does not trigger it
    this.store.subscribeAction(({ type, payload: soundName }) => {
      if (type !== 'playSound') {
        return;
      }

      const { volume = 1.0 } = soundFiles[soundName];
      sounds[soundName].play({ volume });
    });
  }
}
