import { useClickable } from './clickable';

export const useClickableSpecialLighting = (scene, obj, propsData) => {
  const clickable = useClickable(scene, obj, propsData);
  const [clickableData] = clickable;
  const { store } = scene;

  const getAlpha = () => {
    if (store.state.lightsOn) {
      return 1.0;
    } else if (clickableData.isHover) {
      return 0.4;
    } else {
      return 0.2;
    }
  };

  obj.disposable(
    store.watch(
      () => getAlpha(),
      (alpha) => {
        obj.setAlpha(alpha);
      },
      { immediate: true }
    )
  );

  return clickable;
};
