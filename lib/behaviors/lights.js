import { TYPE_LIGHT_SWITCH } from '../constants';
import { getObjectDataValue } from '../utils/phaser-utils';

const hasSpecialLighting = (obj) => {
  // TODO: clean this code smell
  return getObjectDataValue(obj, 'type') === TYPE_LIGHT_SWITCH;
};

export const useLights = (scene) => {
  const { store } = scene;

  store.watch(
    () => store.state.lightsOn,
    (isOn) => {
      const alpha = isOn ? 1 : 0;

      scene.children.list
        .filter((x) => !hasSpecialLighting(x))
        .forEach((x) => x.setAlpha(alpha));
    },
    { immediate: true }
  );
};
