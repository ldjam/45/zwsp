import {
  TYPE_INVENTORY,
  TYPE_LIGHT_SWITCH,
  TYPE_CLICKABLE,
  TYPE_SOCKET
} from '../constants';
import { useLightSwitch } from './light-switch';
import { useInventoryItem } from './inventory-item';
import { useClickable } from './clickable';
import { useSocket } from './socket';

export const useFromType = (type) => {
  switch (type) {
    case TYPE_INVENTORY:
      return useInventoryItem;
    case TYPE_LIGHT_SWITCH:
      return useLightSwitch;
    case TYPE_CLICKABLE:
      return useClickable;
    case TYPE_SOCKET:
      return useSocket;
    default:
      return () => {};
  }
};
