import { getObjectData } from '../utils/phaser-utils';
import { useClickable } from './clickable';

export const useSocket = (scene, obj) => {
  const { socketSwitch = '', socketRequires = '' } = getObjectData(obj, {
    socketSwitch: 'socket_switch',
    socketRequires: 'socket_requires'
  });
  const requires = socketRequires
    ? socketRequires.split(',').map((i) => i.trim())
    : [];

  useClickable(scene, obj, {
    action: 'useSocket',
    actionArg: {
      socketSwitch,
      requires
    }
  });

  obj.disposable(
    scene.store.watch(
      () => scene.store.state.visible,
      (flags) => {
        if (flags.includes(socketSwitch)) {
          obj.destroy();
        }
      }
    )
  );
};
