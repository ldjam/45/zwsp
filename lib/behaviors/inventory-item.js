import { useClickable } from './clickable';

export const useInventoryItem = (scene, obj) => {
  let isDestroying = false;

  const { store } = scene;
  const { name } = obj;

  useClickable(scene, obj, { action: 'pickUp', actionArg: name });

  obj.disposable(
    store.watch(
      () => store.getters.inventoryByName,
      (inventoryByName) => {
        if (inventoryByName[name] && !isDestroying) {
          obj.setVisible(false);

          // delay destroy so visibility can trigger mouse events
          isDestroying = true;
          setTimeout(() => obj.destroy(), 100);
        }
      },
      { immediate: true }
    )
  );
};
