import Phaser from 'phaser';
import { createReactiveObject } from '../utils/vue-utils';
import { getObjectData } from '../utils/phaser-utils';

export const useClickable = (scene, obj, propsData = {}) => {
  const { store } = scene;
  const data = createReactiveObject({ isHover: false });
  const props = {
    ...getObjectData(obj, {
      action: 'click_action',
      actionArg: 'click_action_arg'
    }),
    ...propsData
  };

  obj.setInteractive({ pixelPerfect: true });

  obj.disposable(
    store.watch(
      () => data.isHover,
      (isHover) => {
        if (isHover) {
          const color = Phaser.Display.Color.GetColor32(255, 0, 255, 180);
          obj.setTint(color);
        } else {
          obj.clearTint();
        }
      },
      { immediate: true }
    )
  );

  obj
    .on('pointerdown', () => {
      store.dispatch(props.action, props.actionArg);
    })
    .on('pointerover', () => {
      data.isHover = true;
    })
    .on('pointerout', () => {
      data.isHover = false;
    });

  return [data];
};
