export const toggleVisibleFromColor = (scene, obj, color) => {
  obj.disposable(
    scene.store.watch(
      () => scene.store.getters.enabledColors[color],
      (isVisible) => {
        obj.visible = isVisible;
      },
      { immediate: true }
    )
  );
};
