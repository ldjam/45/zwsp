import { getObjectData } from '../utils/phaser-utils';
import { useClickableSpecialLighting } from './clickable-special-lighting';

export const useLightSwitch = (scene, obj) => {
  const props = getObjectData(obj, { isOn: 'is_on' });

  if (obj.name === 'startArrow') {
    obj.setAlpha(0.7);
  } else {
    useClickableSpecialLighting(scene, obj, { action: 'toggleLights' });
  }

  obj.disposable(
    scene.store.watch(
      () => scene.store.state.lightsOn,
      (isOn) => {
        obj.visible = isOn === props.isOn;
      },
      { immediate: true }
    )
  );
};
