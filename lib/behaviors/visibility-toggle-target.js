import { getObjectData } from '../utils/phaser-utils';
import { createReactiveObject } from '../utils/vue-utils';

export const toggleVisible = (scene, obj) => {
  let timeoutId = 0;

  const props = getObjectData(obj, ['visibilityToggleTarget', 'delay']);
  const delay = props.delay ? Number(props.delay) : 0;
  const data = createReactiveObject({ show: false });

  obj.disposable(
    scene.store.watch(
      () => scene.store.state.visible,
      (visible) => {
        data.show = visible.includes(props.visibilityToggleTarget);
      },
      { immediate: true }
    )
  );

  obj.disposable(
    scene.store.watch(
      () => data.show,
      (show) => {
        clearTimeout(timeoutId);

        if (!show) {
          obj.visible = false;
          return;
        }

        timeoutId = setTimeout(() => {
          obj.visible = true;
        }, delay);
      },
      { immediate: true }
    )
  );
};
