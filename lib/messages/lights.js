import { randomPicker } from '../utils/array-utils';

const TOGGLE_LIGHTS_MESSAGES = [
  "Ah! That's better...",
  'Okay... Now where was I?',
  'I almost fell asleep there...'
];

const getRandomToggleLightsMessage = randomPicker(TOGGLE_LIGHTS_MESSAGES);

export const getToggleLightsMessage = ({ lightsOn, lightsOnCount }) => {
  if (!lightsOn) {
    return '';
  } else if (lightsOnCount === 1) {
    return 'Woah! Who left this mess on the floor?';
  } else if (lightsOnCount > 1) {
    return getRandomToggleLightsMessage();
  } else {
    return '';
  }
};
