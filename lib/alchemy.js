import { isSubset, isEqualSets, isEqualArrays } from './utils/array-utils';

export const recipes = [
  {
    ingredients: new Set(['scissors', 'square_pink']),
    result: 'triangles_pink',
    consumes: ['square_pink']
  },

  {
    ingredients: new Set(['chemicalGreen', 'chemicalBlue']),
    result: 'chemicalCyan',
    consumes: ['chemicalGreen', 'chemicalBlue']
  },

  {
    ingredients: new Set(['chemicalRed', 'chemicalBlue']),
    result: 'chemicalPurple',
    consumes: ['chemicalRed', 'chemicalBlue']
  },

  {
    ingredients: new Set(['chemicalRed', 'chemicalGreen']),
    result: 'chemicalBrown',
    consumes: ['chemicalGreen', 'chemicalRed']
  },

  {
    ingredients: new Set(['chemicalCyan', 'chemicalRed']),
    result: 'chemicalBlack',
    consumes: ['chemicalCyan', 'chemicalRed']
  },

  {
    ingredients: new Set(['chemicalBrown', 'chemicalBlue']),
    result: 'chemicalBlack',
    consumes: ['chemicalBrown', 'chemicalBlue']
  },

  {
    ingredients: new Set(['chemicalPurple', 'chemicalGreen']),
    result: 'chemicalBlack',
    consumes: ['chemicalGreen', 'chemicalPurple']
  },

  {
    ingredients: new Set(['woolGreen', 'woolRed', 'woolBlue', 'buttons']),
    result: 'sweater',
    consumes: ['woolGreen', 'woolRed', 'woolBlue', 'buttons']
  },

  {
    ingredients: new Set(['woolGreen', 'buttons', 'straw']),
    result: 'voodooDoll',
    consumes: ['woolGreen', 'buttons', 'straw']
  },

  {
    ingredients: new Set(['woolRed', 'buttons', 'straw']),
    result: 'voodooDoll',
    consumes: ['woolRed', 'buttons', 'straw']
  },

  {
    ingredients: new Set(['woolBlue', 'buttons', 'straw']),
    result: 'voodooDoll',
    consumes: ['woolBlue', 'buttons', 'straw']
  },

  {
    ingredients: new Set(['piePlate', 'egg', 'frog', 'flour', 'sugar', 'milk']),
    result: 'frogPie',
    consumes: ['piePlate', 'egg', 'frog']
  }
];

export const matchesRecipeOrder = (recipe, selection) => {
  const order = recipe.order || [];

  return !order.length || isEqualArrays(order, selection);
};

export const matchesRecipe = (recipe, selection) => {
  return (
    isEqualSets(recipe.ingredients, selection) &&
    matchesRecipeOrder(recipe, selection)
  );
};

export const findRecipe = (selection) => {
  const recipe = recipes.find((x) => isSubset(selection, x.ingredients));

  if (recipe && !matchesRecipeOrder(recipe, selection)) {
    return null;
  }

  return recipe;
};
