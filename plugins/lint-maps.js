import { MAPS } from '../lib/maps';

Object.entries(MAPS).forEach(([mapName, mapData]) => {
  const objectLayers = mapData.layers.filter((l) => l.type === 'objectgroup');
  const objectLayerNames = objectLayers.map((l) => l.name);
  const uniqueObjectLayerNames = new Set(objectLayerNames);
  if (objectLayerNames.length !== uniqueObjectLayerNames.size) {
    throw new Error(`Map "${mapName}" contains duplicate layer name!`);
  }

  objectLayers.forEach(({ name, objects }) => {
    const objectNames = objects.map((o) => o.name);
    const uniqueObjectNames = new Set(objectNames);
    if (objectNames.length !== uniqueObjectNames.size) {
      throw new Error(
        `Map "${mapName}" contains duplicate object name in layer "${name}"!`
      );
    }

    if (objectNames.some((name) => !name || name === '')) {
      throw new Error(
        `Map "${mapName}" contains empty object name in layer "${name}"!`
      );
    }
  });
});
