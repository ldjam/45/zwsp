import { shallowMount } from '@vue/test-utils';
import Game from '~/components/Game.vue';
import main from '~/lib/main';

jest.mock('~/lib/main');

describe('Game', () => {
  it('calls main when mounted', () => {
    shallowMount(Game);

    expect(main).toHaveBeenCalled();
  });
});
